#Telnet seems to have its own module in python, that's helpful i guess
import telnetlib                #Lets you manipulate telnet.
import sys
import argparse
import yaml


filepath = r'C:\Users\trymk_000\Desktop\Proper\configs\Cisco_config_info.yaml'

def yaml_loader(filepath):              #This function makes coding less tedious, basicly a variable.
    with open(filepath, "r") as file_descriptor:
              data = yaml.load(file_descriptor)
    return data

config = yaml_loader(filepath)   # having 2 seperate variables called data would be confusing, so i changed this one to be config.

parser = argparse.ArgumentParser()
parser.add_argument("--host", help='define an IP address')
args = parser.parse_args()

t = telnetlib.Telnet(config.get('Login').get('HOST'))              #A variable meant to make the rest of the code shorter and easier to read.

if args.host:                                       #if the user wants to enter the IP manually through cmd the defenition of t changes.
    t = telnetlib.Telnet(args.host)
    


def c(string):                          #This function makes writing commands into the code less time-consuming and also less likely to give you cataracts.
    t.write((string).encode('ascii') + b'\n')

def r(string):                          #This function makes reading commands from the code less time-consuming and also less likely to give you cataracts.
    t.read_until(string.encode())

def Login():
    r('Password: ')
    c(config.get('Login').get('Password'))
    r('Switch>')
    c('en')
    r('Password: ')
    c(config.get('Login').get('Adminpassword'))
    c('conf t')
    print ('Login successful.')
    

def getPorts( portdef ):
  if type(portdef) == str :
      
    if portdef[0] == 'L':
      strlst = portdef[1:].split(',')
      return [int(number) for number in strlst]
      
    elif portdef[0] == 'R':
      strrng = portdef[1:].split('-')
      return [x for x in range(int(strrng[0]), int(strrng[1])+1)]
      
    elif int(portdef) > int(config.get('Specs').get('Ports')) + 1:
        return 'invalid string'
    
    else:
        return 'invalid string'
    
  else:
      return 'That is not a string!'


Vlist = config.get('Vlans')

def GetVlanId(vlan):
    return (vlan[4:])

def VlanConfigAuto():
    for vlan in Vlist:
        for port in getPorts(Vlist.get(str(vlan))):
            c('int fa1/0/' + str(port))
            r('Switch(config-if)#')
            c('switchport access vlan ' + (GetVlanId(vlan)))
            r('Switch(config-if)#')
    print ('Vlans configured.')


def TrunkConfigAuto():
    trunk = config.get('Specs').get('TrunkPort')
    c('int fa1/0/' + trunk)
    r('Switch(config-if)#')
    c('switchport trunk encapsulation dot1q')
    r('Switch(config-if)#')
    c('switchport trunk allowed vlan 1-4000')
    r('Switch(config-if)#')
    c('switchport mode trunk')
    r('Switch(config-if)#')   
    print ('Trunk configured.')   

def RoutingConfigAuto():
    for vlan in Vlist:
        c('int vlan ' +  (vlan[4:]))
        r('Switch(config-if)#')
        c('ip address 10.0.' + (str(GetVlanId(vlan))) + '.1 255.255.255.0')
        r('Switch(config-if)#')
    print ('Routing configured.')
    
Login()
VlanConfigAuto()
TrunkConfigAuto()
RoutingConfigAuto()
print('Done')