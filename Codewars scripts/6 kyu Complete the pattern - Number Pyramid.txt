###Task:

You have to write a function pattern which creates the following Pattern(See Examples) upto n(parameter) number of rows.

####Rules/Note:

If the Argument is 0 or a Negative Integer then it should return "" i.e. empty string.
All the lines in the pattern have same length i.e equal to the number of characters in the last line.
Range of n is (-8,100]
###Examples:

pattern(5):

    1    
   121   
  12321  
 1234321 
123454321



My code:

def pattern(n):
  string = ""
  number = "1"
  i = 1
  while i < n + 1:
      string += " " * (n - i)
      for e in range(1,i + 1):
        string += str(str(e)[len(str(e)) - 1])
      for e in range(i-1,0,-1):
        string += str(str(e)[len(str(e)) - 1])
      if i == n:
        string += " " * (n - i)
      else:
        string += " " * (n - i) + "\n"
      i += 1
      number += "1"
  return string


that str(str(e)[len(str(e)) - 1]) thing is probably more complicated than it has to be.

Best code:

def pattern(n):
    output = []
    for i in range (1, n + 1):
        wing = ' ' * (n - i) + ''.join(str(d % 10) for d in range(1, i))
        output.append(wing + str(i % 10) + wing[::-1])
    return '\n'.join(output)


Moduloing the number was pretty smart.
That made it so you only needed 2 loops, instead of my 3 loops.