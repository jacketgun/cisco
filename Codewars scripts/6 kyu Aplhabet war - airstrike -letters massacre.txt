Introduction
There is a war and nobody knows - the alphabet war!
There are two groups of hostile letters. The tension between left side letters and right side letters was too high and the war began. The letters called airstrike to help them in war - dashes and dots are spreaded everywhere on the battlefield.

Task
Write a function that accepts fight string consists of only small letters and * which means a bomb drop place. Return who wins the fight. When the left side wins return Left side wins!, when the right side wins return Right side wins!, in other case return Let's fight again!.

The left side letters and their power:

 w - 4
 p - 3 
 b - 2
 s - 1
The right side letters and their power:

 m - 4
 q - 3 
 d - 2
 z - 1
The other letters don't have power and are only victims.
The * bombs kills the adjacent letters ( i.e. aa*aa => a___a, **aa** => ______ );


Example
AlphabetWar("s*zz");           //=> Right side wins!
AlphabetWar("*zd*qm*wp*bs*"); //=> Let's fight again!
AlphabetWar("zzzz*s*");       //=> Right side wins!
AlphabetWar("www*www****z");  //=> Left side wins!


My code:
i threw in the towel on this one, what even are these solutions?
there are no 2 people with the same solution either....


Best code:

def alphabet_war(fight):

  s = 'wpbsszdqm'
  alive = []

  if '*' not in fight[:2]:
    alive.append(fight[0])

  for i, c  in enumerate(fight[1:], 1):
    if '*' not in fight[i-1:i+2]:
      alive.append(c)
  
  x = sum(4 - s.index(c) for c in alive if c in s)
  return 'Let\'s fight again!' if x==0 else ['Left','Right'][x<0] + ' side wins!'

the difficulty of these katas are all over the place, this was supposed to be the same difficulty as the multiples of 3 and 5 kata?!